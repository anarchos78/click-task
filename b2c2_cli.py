# -*- coding: utf-8 -*-

"""
.. module:: .b2c2.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import sys

import click

from tabulate import tabulate

from b2c2.b2c2communicate import B2C2Communicate


@click.command()
@click.option('--token', help='B2C2 authentication token.')
def cli(token):
    """A command line interface to trade with B2C2 platform."""

    # Print out an intro text
    click.echo(
        click.style(
            'This simple cli program will help '
            'you to trade with B2C2 platform.',
            bold=True
        )
    )

    # Wait for a key press
    click.echo('\n')
    click.pause()
    click.echo('\n')

    if not token:
        token = click.prompt(
            text=click.style(
                'Please enter a B2C2 token',
                fg='green'
            )
        )

    # Instantiate the "B2C2Communicate" class
    c = B2C2Communicate(api_token=token)

    click.echo('\n')
    click.pause()
    click.echo('\n')

    choices_basic = {
        1: 'Get balance',
        2: 'Get tradable instruments',
        3: 'Get all ledger entries',
        4: 'Get all trades',
        5: 'Request for quote',
        6: 'Quit'
    }

    options_basic = [str(k) for k in choices_basic.keys()]

    while True:
        choice = click.prompt(
            text=click.style(
                'Your options are:\n'
                '1. "{ch1}"\n'
                '2. "{ch2}"\n'
                '3. "{ch3}"\n'
                '4. "{ch4}"\n'
                '5. "{ch5}"\n'
                '6. "{ch6}"\n'
                'Please select from "{options}"'.format(
                    options=', '.join(options_basic),
                    ch1=choices_basic[1],
                    ch2=choices_basic[2],
                    ch3=choices_basic[3],
                    ch4=choices_basic[4],
                    ch5=choices_basic[5],
                    ch6=choices_basic[6]
                ),
                fg='green'
            )
        )
        print('\n')

        if choice in options_basic:
            if choice == '1':
                print('Your balance is:')
                print(
                    tabulate(
                        [[k, v] for k, v in c.balance().iteritems()],
                        headers=['Currency', 'Balance'],
                        tablefmt='orgtbl'
                    )
                )
                print('\n')
            elif choice == '2':
                print('Your tradable instruments are:')
                print(
                    tabulate(
                        [[inst] for inst in c.instruments()],
                        headers=['Instrument'],
                        tablefmt='orgtbl'
                    )
                )
                print('\n')
            elif choice == '3':
                print('Your ledger entries are:')
                print(
                    tabulate(
                        [
                            [
                                entry.get('amount'),
                                entry.get('created'),
                                entry.get('currency'),
                                entry.get('reference'),
                                entry.get('transaction_id'),
                                entry.get('type')
                            ]
                            for entry
                            in c.ledger()
                        ],
                        headers=[
                            'Amount',
                            'Created',
                            'Currency',
                            'Reference',
                            'Transaction ID',
                            'Type'
                        ],
                        tablefmt='orgtbl'
                    )
                )
                print('\n')
            elif choice == '4':
                print('Your trades are:')
                print(
                    tabulate(
                        [
                            [
                                trade.get('created'),
                                trade.get('instrument'),
                                trade.get('price'),
                                trade.get('quantity'),
                                trade.get('rfq_id'),
                                trade.get('side'),
                                trade.get('trade_id')
                            ]
                            for trade
                            in c.trade_get()
                        ],
                        headers=[
                            'Created',
                            'Instrument',
                            'Price',
                            'Quantity',
                            'RFQ ID',
                            'Side',
                            'Trade ID'
                        ],
                        tablefmt='orgtbl'
                    )
                )
                print('\n')
            elif choice == '5':
                instruments = c.instruments()
                instruments_choices = [
                    '{i}. "{instrument}"'.format(
                        i=i + 1,
                        instrument=instrument
                    )
                    for i, instrument in enumerate(instruments)
                ]
                options_instruments = [
                    str(k) for k
                    in xrange(1, len(instruments) + 1)
                ]

                while True:
                    instrument_rfq = click.prompt(
                        text=click.style(
                            'Available instruments:\n'
                            '{instruments_choices}\n'
                            'Please select'.format(
                                instruments_choices='\n'.join(
                                    instruments_choices
                                )
                            ),
                            fg='green'
                        )
                    )
                    print('\n')

                    if instrument_rfq in options_instruments:
                        break
                    elif instrument_rfq not in instruments_choices:
                        click.echo(
                            click.style(
                                'Not valid choice [{}]'.format(instrument_rfq),
                                fg='red'
                            )
                        )

                choices_side = {
                    1: 'Buy',
                    2: 'Sell'
                }
                options_side = [str(k) for k in choices_side.keys()]

                while True:
                    side_rfq = click.prompt(
                        text=click.style(
                            'Your options are:\n'
                            '1. "{ch1}"\n'
                            '2. "{ch2}"\n'
                            'Please select from "{options}"'.format(
                                options=', '.join(options_side),
                                ch1=choices_side[1],
                                ch2=choices_side[2]
                            ),
                            fg='green'
                        )
                    )
                    print('\n')

                    if side_rfq in options_side:
                        break
                    elif side_rfq not in options_side:
                        click.echo(
                            click.style(
                                'Not valid choice [{}]'.format(side_rfq),
                                fg='red'
                            )
                        )

                while True:
                    quantity_rfq = click.prompt(
                        text=click.style(
                            'What\'s the amount that you want to {}'.format(
                                choices_side[int(side_rfq)].lower()
                            ),
                            fg='green'
                        )
                    )
                    print('\n')

                    if quantity_rfq.isdigit() and int(quantity_rfq) > 0:
                        break
                    elif not quantity_rfq.isdigit() or int(quantity_rfq) <= 0:
                        click.echo(
                            click.style(
                                'Not valid choice [{}]'.format(quantity_rfq),
                                fg='red'
                            )
                        )

                rfq = c.request_for_quote(
                    quantity=quantity_rfq,
                    side=choices_side[int(side_rfq)].lower(),
                    instrument=instruments[int(instrument_rfq) - 1]
                )

                print('Your quote is:')
                print(
                    tabulate(
                        [
                            [
                                rfq.get('client_rfq_id'),
                                rfq.get('instrument'),
                                rfq.get('price'),
                                rfq.get('quantity'),
                                rfq.get('rfq_id'),
                                rfq.get('side'),
                                rfq.get('valid_until')
                            ]
                        ],
                        headers=[
                            'Client RFQ',
                            'Instrument',
                            'Price',
                            'Quantity',
                            'RFQ ID',
                            'Side',
                            'Valid Until'
                        ],
                        tablefmt='orgtbl'
                    )
                )
                print('\n')

                choices_execute_rfq = {
                    1: 'Yes',
                    2: 'No'
                }
                options_execute_rfq = [str(k) for k in choices_side.keys()]

                while True:
                    execute_rfq = click.prompt(
                        text=click.style(
                            'Do you want to execute the trade?\n'
                            '1. "{ch1}"\n'
                            '2. "{ch2}"\n'
                            'Please select from '
                            '"{options_execute_rfq}"'.format(
                                options_execute_rfq=', '.join(options_side),
                                ch1=choices_execute_rfq[1],
                                ch2=choices_execute_rfq[2]
                            ),
                            fg='green'
                        )
                    )
                    print('\n')

                    if execute_rfq in options_execute_rfq:
                        break
                    elif execute_rfq not in options_execute_rfq:
                        click.echo(
                            click.style(
                                'Not valid choice [{}]'.format(execute_rfq),
                                fg='red'
                            )
                        )

                if execute_rfq == '2':
                    print('Back to the basic menu...\n')
                    continue
                else:
                    print('Executing the trade...\n')

                    while True:
                        if c.execution_td(rfq.get('valid_until')):
                            response_trade = c.trade_post(
                                rfq_id=rfq.get('rfq_id'),
                                quantity=rfq.get('quantity'),
                                side=rfq.get('side'),
                                instrument=rfq.get('instrument'),
                                price=rfq.get('price')
                            )

                            print('Your trade details are:')
                            print(
                                tabulate(
                                    [
                                        [
                                            response_trade.get('created'),
                                            response_trade.get('price'),
                                            response_trade.get('instrument'),
                                            response_trade.get('trade_id'),
                                            response_trade.get('rfq_id'),
                                            response_trade.get('side'),
                                            response_trade.get('quantity')
                                        ]
                                    ],
                                    headers=[
                                        'Created',
                                        'Price',
                                        'Instrument',
                                        'Trade ID',
                                        'RFQ ID',
                                        'Side',
                                        'Quantity'
                                    ],
                                    tablefmt='orgtbl'
                                )
                            )
                            print('\n')
                            print('Back to the basic menu...\n')
                            break
                        else:
                            print(
                                'You have only 15 seconds '
                                'to execute the trade. \n'
                                'Back to the basic menu...\n'
                            )
                            break
            elif choice == '6':
                click.echo('Bye bye.')
                sys.exit(0)
        elif choice not in options_basic:
            click.echo(
                click.style(
                    'Not valid choice [{}]'.format(choice),
                    fg='red'
                )
            )
