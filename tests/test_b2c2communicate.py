# -*- coding: utf-8 -*-

"""
.. module:: .test_b2c2communicate.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import json
import uuid
import unittest

import mock
import requests

from b2c2.b2c2communicate import B2C2Communicate


class TestB2C2Communicate(unittest.TestCase):
    """Tests B2C2Communicate"""

    def setUp(self):
        patcher_get = mock.patch('b2c2.b2c2communicate.requests.get')

        self.client = B2C2Communicate()
        self.mock_response = mock.Mock()
        self.mock_response = mock.Mock(status_code=200)
        self.mock_response.json.return_value = None
        self.mock_response.raise_for_status.return_value = None

        self.uuid = str(uuid.uuid4())
        self.quantity = '1'
        self.side = 'buy'
        self.instrument = 'BTCUSD'
        self.price = '666.000'
        self.amount = '10'
        self.currency = 'BTC'

        self.mock_request = patcher_get.start()
        self.mock_request.return_value = self.mock_response

        self.balance = {
            "USD": "0",
            "BTC": "0",
            "JPY": "0",
            "GBP": "0",
            "ETH": "0",
            "EUR": "0",
            "CAD": "0"
        }

        self.instruments = [
              {
                "name": "BTCUSD"
              },
              {
                "name": "BTCEUR"
              },
              {
                "name": "BTCGBP"
              },
              {
                "name": "ETHBTC"
              },
              {
                "name": "ETHUSD"
              },
              {
                "name": "EURUSD"
              }
        ]

        self.ledger = [
              {
                "transaction_id": "ef69dz9f-5ade-4aa4-87c2-e2eaef37d8bc",
                "created": "2016-12-14T14:17:03Z",
                "reference": "2ff83923-63f0-45bd-959b-ece69eb72636",
                "currency": "BTC",
                "amount": "1.00000000",
                "type": "trade"
              },
              {
                "transaction_id": "393f9373-8a9a-478b-8669-6fc443a36780",
                "created": "2017-03-10T17:17:56Z",
                "reference": "Example",
                "currency": "EUR",
                "amount": "100000.00000000",
                "type": "transfer"
              }
        ]

        self.get_trade = [
              {
                "created": "2016-09-27T11:27:46.599039Z",
                "trade_id": "2ff83945-63f0-45bc-959b-ece69eb72636",
                "rfq_id": "db0585d0-3696-4d94-bdc7-309b351806ff",
                "quantity": "20.0000000000",
                "side": "buy",
                "instrument": "BTCUSD",
                "price": "700.00000000"
              },
              {
                "created": "2016-09-27T11:27:49.599039Z",
                "trade_id": "049fb5d2-7d87-403z-a59e-49612b8aec92",
                "rfq_id": "95037d3a-1179-4fd9-b2f8-55a13c0efbb3",
                "quantity": "100.0000000000",
                "side": "sell",
                "instrument": "ETHUSD",
                "price": "15.00000000"
              }
        ]

        self.endpoint_get_results = {
                "balance": json.dumps(self.balance),
                "instruments": json.dumps(self.instruments),
                "ledger": json.dumps(self.ledger),
                "trade": json.dumps(self.get_trade)
        }

        self.rfq = {
                "valid_until": "2017-01-01T19:45:22.025464Z",
                "rfq_id": "d4e41399-e7a1-4576-9b46-349420040e1a",
                "client_rfq_id": "149dc3e7-4e30-4e1a-bb9c-9c30bd8f5ec7",
                "quantity": "1.0000000000",
                "side": "buy",
                "instrument": "BTCUSD",
                "price": "700.00000000"
        }

        self.post_trade = {
                "created": "2016-09-27T11:27:46.599039Z",
                "price": "700.0000000000",
                "instrument": "BTCUSD",
                "trade_id": "5c7e90cc-a8d6-4db5-8348-44053b2dcbdf",
                "rfq_id": "f7492962-783e-45c7-ae81-6eb61f4d7251",
                "side": "buy",
                "quantity": "1.0000000000"
        }

        self.withdrawal = {
                "amount": "10.0000000000",
                "currency": "BTC",
                "withdrawal_id": "5c7e90cc-a8d6-4db5-8348-44053b2dcbdf",
        }

        self.endpoint_post_results = {
                "request_for_quote": self.rfq,
                "trade": self.post_trade,
                "withdrawal": self.withdrawal
        }

    def tearDown(self):
        self.mock_request.stop()

    @mock.patch('b2c2.b2c2communicate.requests.get')
    def test_get(self, mock_get):
        """Tests _get endpoints"""

        for call_count, endpoint in enumerate(self.endpoint_get_results):
            self.mock_response.json.return_value = \
                self.endpoint_get_results[endpoint]

            # Assign our mock response as the result of our patched function
            mock_get.return_value = self.mock_response

            result = self.client._get(resource=endpoint)

            # Check that our function made the expected internal calls
            self.assertEqual(
                call_count + 1,
                self.mock_response.json.call_count
            )
            # If we want, we can check the contents of the response
            self.assertEqual(result, self.endpoint_get_results[endpoint])

    @mock.patch('b2c2.b2c2communicate.requests.get')
    def test_get_error(self, mock_get):
        """Tests bad _get request"""

        for call_count, endpoint in enumerate(self.endpoint_get_results):
            http_error = requests.exceptions.HTTPError()
            self.mock_response.raise_for_status.side_effect = http_error

            # Assign our mock response as the result of our patched function
            mock_get.return_value = self.mock_response

            result = self.client._get(resource=endpoint)

            self.assertEqual(result, None)

            # Check that our function made the expected internal calls
            self.assertEqual(
                call_count + 1,
                self.mock_response.raise_for_status.call_count
            )

            # Make sure we did not attempt to deserialize the response
            self.assertEqual(0, self.mock_response.json.call_count)

    @mock.patch('b2c2.b2c2communicate.requests.post')
    def test_post(self, mock_post):
        """Tests _post endpoints"""

        for call_count, endpoint in enumerate(self.endpoint_post_results):
            self.mock_response.json.return_value = \
                self.endpoint_post_results[endpoint]

            # Assign our mock response as the result of our patched function
            mock_post.return_value = self.mock_response

            params = None

            if endpoint == 'request_for_quote':
                params = {
                    'quantity': self.quantity,
                    'side': self.side,
                    'instrument': self.instrument
                }
            elif endpoint == 'trade':
                params = {
                    'rfq_id': self.uuid,
                    'quantity': self.quantity,
                    'side': self.side,
                    'instrument': self.instrument,
                    'price': self.price
                }
            elif endpoint == 'withdrawal':
                params = {
                    'amount': self.amount,
                    'currency': self.currency
                }

            result = self.client._post(resource=endpoint, **params)

            # Check that our function made the expected internal calls
            self.assertEqual(
                call_count + 1,
                self.mock_response.json.call_count
            )
            # If we want, we can check the contents of the response
            self.assertEqual(result, self.endpoint_post_results[endpoint])

    @mock.patch('b2c2.b2c2communicate.requests.post')
    def test_post_error(self, mock_post):
        """Tests bad _get request"""

        for call_count, endpoint in enumerate(self.endpoint_post_results):
            http_error = requests.exceptions.HTTPError()
            self.mock_response.raise_for_status.side_effect = http_error

            # Assign our mock response as the result of our patched function
            mock_post.return_value = self.mock_response

            params = None

            if endpoint == 'request_for_quote':
                params = {
                    'quantity': self.quantity,
                    'side': self.side,
                    'instrument': self.instrument
                }
            elif endpoint == 'trade':
                params = {
                    'rfq_id': self.uuid,
                    'quantity': self.quantity,
                    'side': self.side,
                    'instrument': self.instrument,
                    'price': self.price
                }
            elif endpoint == 'withdrawal':
                params = {
                    'amount': self.amount,
                    'currency': self.currency
                }

            result = self.client._post(resource=endpoint, **params)

            self.assertEqual(result, None)

            # Check that our function made the expected internal calls
            self.assertEqual(
                call_count + 1,
                self.mock_response.raise_for_status.call_count
            )

            # Make sure we did not attempt to deserialize the response
            self.assertEqual(0, self.mock_response.json.call_count)
