# -*- coding: utf-8 -*-

"""
.. module:: .setup.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from setuptools import setup


setup(
    name='b2c2',
    version='1.0',
    py_modules='b2c2',
    install_requires=[
        'click==6.7',
        'mock==2.0.0',
        'requests==2.18.4',
        'tabulate==0.8.1'
    ],
    entry_points="""
        [console_scripts]
        b2c2=b2c2_cli:cli
    """
)
