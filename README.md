#### A simple command line app to comunicate with https://sandboxapi.b2c2.ne API (*you need a valid token to gain access to API*)

##### *How to run the app*  
Open a terminal and:

1. Clone the repo `git clone https://anarchos78@bitbucket.org/anarchos78/click-task.git`  
2. Go into the application directory `cd click-task`  
3. Create a virtual environment for the project, using python 2.7 and activate it (I prefer ***pyenv***)  
4. Run `pip install --editable .` to install the module 
5. Run the tests in the projects directory issue the command `python -m unittest -v tests.test_b2c2communicate`  
6. To run the program issue to the cli prompt `b2c2`  

**Enjoy!**
