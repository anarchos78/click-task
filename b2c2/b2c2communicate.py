# -*- coding: utf-8 -*-

"""
.. module:: .B2C2Communicate.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import json
import uuid
import datetime

import requests
import logging


logging.basicConfig()
logger = logging.getLogger()


class B2C2Communicate(object):
    """A class responsible to communicate with B2C2 API"""

    def __init__(self, root_endpoint='https://sandboxapi.b2c2.net',
                 api_token=None):
        """The constructor"""

        self.root_endpoint = root_endpoint
        self.headers = {
            'Authorization': 'Token {}'.format(api_token),
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X '
                          '10.12; rv:55.0) Gecko/20100101 Firefox/55.0'
        }

    def _get(self, resource, **kwargs):
        """
        Gets account resources of: balance, instruments, ledger, trade
        :return: json
        """

        if kwargs:
            offset = kwargs.get('offset')
            limit = kwargs.get('limit')

            if limit > 100:
                logging.warning('The maximum requested result records are 100')
                limit = 100

            response = requests.get(
                'https://sandboxapi.b2c2.net/{}/'.format(resource),
                headers=self.headers,
                params={
                    'offset': offset,
                    'limit': limit
                }
            )
        else:
            response = requests.get(
                'https://sandboxapi.b2c2.net/{}/'.format(resource),
                headers=self.headers
            )

        try:
            response.raise_for_status()

            return response.json()
        except requests.exceptions.HTTPError as e:
            logger.error(e)

    def _post(self, resource, **kwargs):
        """
        Post to account resources of: request_for_quote, trade, withdrawal
        :return: json
        """
        post_data = None

        if resource == 'request_for_quote':
            post_data = {
                'client_rfq_id': str(uuid.uuid4()),
                'quantity': kwargs['quantity'],
                'side': kwargs['side'],
                'instrument': kwargs['instrument']
            }
        elif resource == 'trade':
            post_data = {
                'rfq_id': kwargs['rfq_id'],
                'quantity': kwargs['quantity'],
                'side': kwargs['side'],
                'instrument': kwargs['instrument'],
                'price': kwargs['price']
            }
        elif resource == 'withdrawal':
            post_data = {
                'amount': kwargs['amount'],
                'currency': kwargs['currency']
            }

        response = requests.post(
                'https://sandboxapi.b2c2.net/{}/'.format(resource),
                data=json.dumps(post_data),
                headers=self.headers
        )

        try:
            response.raise_for_status()

            return response.json()
        except requests.exceptions.HTTPError as e:
            logger.error(e)

    def balance(self):
        """Get account holder balance"""

        return self._get(resource='balance')

    def instruments(self):
        """Get available instruments"""

        instrument_list = [
            list(instr.values())[0] for instr
            in self._get(resource='instruments')
        ]
        return instrument_list

    def ledger(self):
        """Entries affecting balance"""

        return self._get(resource='ledger')

    def trade_get(self, offset=0, limit=50):
        """Get executed trades"""

        kwargs = {
            'offset': offset,
            'limit': limit
        }

        return self._get(resource='trade', **kwargs)

    def request_for_quote(self, quantity, side, instrument):
        """Request for Quotes. Return a Quote,
           with a price at which a trade can be executed"""

        kwargs = {
            'quantity': quantity,
            'side': side,
            'instrument': instrument
        }

        return self._post('request_for_quote', **kwargs)

    def trade_post(self, rfq_id, quantity, side, instrument, price):
        """Request the execution a trade on the back of a Quote response"""

        kwargs = {
            'rfq_id': rfq_id,
            'quantity': quantity,
            'side': side,
            'instrument': instrument,
            'price': price
        }

        return self._post('trade', **kwargs)

    def withdrawal(self, amount, currency):
        """Request a settlement to an external account"""

        kwargs = {
            'amount': str(amount),
            'currency': currency
        }

        return self._post('withdrawal', **kwargs)

    @staticmethod
    def execution_td(valid_until_time):
        """
        Returns the time difference between
        RFQ valid time and trade execution time in seconds
        """

        if 'Z' in valid_until_time:
            valid_until_time = valid_until_time.replace('Z', '')

        if datetime.datetime.utcnow().isoformat() < valid_until_time:
            return True
        else:
            return False
